package com.company.coinchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by wagner on 26/06/17.
 */

public class CoinChange {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter amount: ");
        int amount = scan.nextInt();

        System.out.println("Enter coins values, and a dot when finish it:");
        List<Double> coins = new ArrayList<>();
        while (scan.hasNextDouble()) {
            coins.add(scan.nextDouble());
        }

        System.out.println(change(amount, coins));
    }

    public static int change(int amount, List<Double> coins) {
        int[] combinations = new int[amount + 1];

        combinations[0] = 1; //There's always 1 way to make $0

        //"Degenerate" cases
        if (amount == 0) { return 1; }
        if (coins.size() == 0) { return 0; }

        for (Double coin : coins) {
            for (int i = 1; i < combinations.length; i++) {
                if (i >= coin) {
                    combinations[i] += combinations[i - coin.intValue()];
                    printAmount(combinations);
                }
            }
            System.out.println();
        }

        return combinations[amount];
    }

    public static void printAmount(int[] arr) {
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
